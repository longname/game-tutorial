package tutorial.main;

import java.awt.Color;
import java.awt.Graphics;

// Does not extend GameObject class.
public class HUD
{
	
	public static int health = 100;		// Yes it should be encapsulated
	
	private int score = 0;
	private int level = 1;
	
	private int greenVal = 255;
	
	public void tick()
	{
		health = (int)Game.clamp(health, 0, 100);		// Keeps health between 0 and 100
		greenVal = health * 25 / 10;
		greenVal = (int)Game.clamp(greenVal, 0, 255);
		if (health > 0)
			score++;
	}
	
// Draws the health bar
	public void render(Graphics g)
	{
		g.setColor(Color.gray);
		g.fillRect(15, 15, 202, 8);
		g.setColor(Color.red);
		g.fillRect(16, 16, 200, 6);
		g.setColor(new Color(75, greenVal, 0));
		g.fillRect(16, 16, health * 2, 6);
		g.setColor(Color.green);
		g.drawString("Score: " + score, 15, 35);
		g.drawString("Level: " + level, 15, 45);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
}
