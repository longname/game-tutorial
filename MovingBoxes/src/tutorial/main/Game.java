package tutorial.main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Random;


// I'm making this game using this: https://www.youtube.com/playlist?list=PLWms45O3n--6TvZmtFHaCWRZwEqnz2MHa
// tutorial. The object is to learn basic concepts for coding games and to use as a starting point for more
// complex personal projects...
public class Game extends Canvas  implements Runnable
{
	private static final long serialVersionUID = -7522931812830250103L;		// Somehow highly recommended.
																			// Will look it up
	public static final  int width = 640, height = width / 12 * 9;
	
	private Thread thread;
	private boolean running = false;
	private Handler handler;
	private Random r;
	private HUD hud;
	private Spawn spawner;
	
	///////////////////////////////////////////////////////////////////////
	
	public Game()
	{
		r = new Random();
		
		hud = new HUD();
		handler = new Handler();
		spawner = new Spawn(handler, hud);
		
		new Window(width, height, "Boxes", this);
		
		
		handler.addObject(new Player(width / 2 - 16, height / 2 - 16, ID.Player, handler));	// Creates a player object

		handler.addObject(new BasicEnemy(r.nextInt(width - 16),r.nextInt(height - 32), ID.BasicEnemy, handler));

		
		this.addKeyListener(new KeyInput(handler));		// Key listener listens for keys. Obviously...
	}

	
// Synchronized methods keep object invocations nice and separate
// The start method is called in the Window class
	public synchronized void start()
	{
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop()
	{
		try
		{
			thread.join();
			running = false;
		}
		catch (Exception e)
		{
			e.fillInStackTrace();
		}
	}
	
	
// The run method is called when the Game class is used to crate a thread, because the class implements Runnable
	public void run()
	{
		this.requestFocus();			// Handy stuff
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;	// How many ticks are called per second
		double ns = 1000000000 / amountOfTicks;
		double delta = 0.0;
		long timer = System.currentTimeMillis();
		int frames = 0;					// Fps counter
			
// This while loop is a game loop
		while (running)
		{
			long now = System.nanoTime();		// nanoTime() gives time elapsed from some arbitrary point after
			delta += (now - lastTime) / ns;		// starting the Java virtual machine in nanoseconds.
			lastTime = now;

// When 1/60th of a second is accumulated do a tick. An if might be sufficient
			while (delta >= 1.0)
			{
				tick();
				delta--;
			}
			
			if (running)
				render();
			
			frames++;
			
			if (System.currentTimeMillis() - timer > 1000)		// Print fps every second in to the console
			{
				timer += 1000;
				System.out.println("FPS: " + frames);
				frames = 0;
			}
		}
		stop();
	}
	
// Invokes ticks 
	private void tick()
	{
		handler.tick();
		hud.tick();
		spawner.tick();
	}
	
// Some funky stuff. Will look it up...
	private void render()
	{
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null)
		{
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, width, height);
		
		handler.render(g);
		hud.render(g);
		
		g.dispose();
		bs.show();
	}
	
// Very handy method for stopping thing at the edges and such
	public static float clamp(float var, float min, float max)
	{
		if (var <= min)
			return min;
		else
			if (var >= max)
				return max;
			else
				return var;
	}
	
	public static void main(String[] args)
	{
		new Game();
	}
}
