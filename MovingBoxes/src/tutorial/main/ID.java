package tutorial.main;

// Enumerator classes generate unique id's
public enum ID 
{
	Player(),
	BasicEnemy(),
	FastEnemy(),
	SmartEnemy(),
	EnemyBoss(),
	Trail();
}
