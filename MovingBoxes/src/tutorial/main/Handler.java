package tutorial.main;

import java.awt.Graphics;
import java.util.LinkedList;

// The Handler class stores all game objects(instances of GameObject class) in a LinkedList and coordinates
//tick and render methods
public class Handler 
{
	LinkedList<GameObject> object = new LinkedList<>();

	
// Takes care of "physics" and stuff according to each objects inner tick method
	public void tick()
	{
		for (int i = 0; i < object.size(); i++)
		{
			GameObject tempObject = object.get(i);
			tempObject.tick();
		}
	}
	
// Draws objects according to their inner render methods
	public void render(Graphics g)
	{
		for (int i = 0; i < object.size(); i++)
		{
			GameObject tempObject = object.get(i);
			tempObject.render(g);
		}
	}
	
	public void clearEnamies()
	{
		for (int i = 0; i < object.size(); i++)
		{
			GameObject tempObject = object.get(i);
			if (tempObject.id == ID.BasicEnemy || tempObject.id == ID.SmartEnemy || tempObject.id == ID.FastEnemy)
			{
				removeObject(tempObject);
				i--;
			}
		}
	}
	
	public void addObject(GameObject object)
	{
		this.object.add(object);
	}
	
	public void removeObject(GameObject object)
	{
		this.object.remove(object);
	}
}