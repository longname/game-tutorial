package tutorial.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Player extends GameObject
{
	Handler handler;
	
	public Player(float x, float y, ID id, Handler handler)
	{
		super(x, y, id);
		this.handler = handler;
	}
	
	private void collision()
	{
		for (int i = 0; i < handler.object.size(); i++)
		{
			GameObject tempObject = handler.object.get(i);
			if ((tempObject.id == ID.BasicEnemy || tempObject.id == ID.FastEnemy || tempObject.id == ID.SmartEnemy)
			&& getBounds().intersects(tempObject.getBounds()))
			{
				HUD.health -= 2;
			}
			if (tempObject.id == ID.EnemyBoss && getBounds().intersects(tempObject.getBounds()))
				HUD.health -= 5;
		}
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle((int)x, (int)y, 32, 32);
	}

	public void tick()
	{
		x += velX;
		y += velY;
		x = Game.clamp(x, 0, Game.width - 36);		//
		y = Game.clamp(y, 0, Game.height - 60);		// Keeps the player in the window
		collision();
	}

	public void render(Graphics g)
	{
		g.setColor(Color.white);
		g.fillRect((int)x, (int)y, 32, 32);
	}
}
