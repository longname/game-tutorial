package tutorial.main;

import java.util.Random;



public class Spawn
{
	

	private Handler handler;
	private HUD hud;
	
	private int scoreKeep = 0;
	private Random r = new Random();
	
	
	
	public Spawn(Handler handler, HUD hud)
	{
		this.handler = handler;
		this.hud = hud;
	}
	
	
	public void tick()
	{
		scoreKeep++;
		if (scoreKeep >= 250)
		{
			scoreKeep = 0;
			hud.setLevel(hud.getLevel() + 1);
			int level = hud.getLevel();
		
			switch (level)
			{
			case 2:
				handler.object.add(new BasicEnemy(r.nextInt(Game.width - 50), r.nextInt(Game.height - 50), ID.BasicEnemy, handler));
				break;
			case 3:
				handler.object.add(new BasicEnemy(r.nextInt(Game.width - 50), r.nextInt(Game.height - 50), ID.BasicEnemy, handler));
				handler.object.add(new BasicEnemy(r.nextInt(Game.width - 50), r.nextInt(Game.height - 50), ID.BasicEnemy, handler));
				break;
			case 4:
				handler.object.add(new FastEnemy(r.nextInt(Game.width - 50), r.nextInt(Game.height - 50), ID.FastEnemy, handler));
				break;
			case 5:
				handler.object.add(new SmartEnemy(r.nextInt(Game.width - 50), r.nextInt(Game.height - 50), ID.SmartEnemy, handler));
				break;
			case 10:
				handler.clearEnamies();
				handler.object.add(new EnemyBoss((Game.width / 2) -64, -138, ID.EnemyBoss, handler));
				break;
			}
		}
	}

}
