package tutorial.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

// A class for the first enemy type
public class EnemyBossBullet extends GameObject
{
	
	private Handler handler;
	Random r = new Random();
	
	public EnemyBossBullet(float x, float y, ID id, Handler handler)
	{
		super(x, y, id);
		velX = r.nextInt(6 - -6) - 6;
		velY = 5;
		this.handler = handler;
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle((int)x, (int)y, 16, 16);
	}
	
	public void tick()
	{
		x += velX;
		y += velY;
		if (x <= 0 || x >= Game.width - 16)		// Reverses x axis movement when roughly at the edge of the window
			velX *= -1;
		if (y <= 0 || y >= Game.height - 32)	// Reverses y axis movement when roughly at the edge of the window
			handler.removeObject(this);
		handler.addObject(new Trail(x, y, 16, 16, ID.Trail, Color.red, 0.03F, handler));
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.RED);
		g.fillRect((int)x, (int)y, 16, 16);
	}
	
}
