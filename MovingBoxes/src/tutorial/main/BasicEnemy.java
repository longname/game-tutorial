package tutorial.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

// A class for the first enemy type
public class BasicEnemy extends GameObject
{
	
	private Handler handler;
	
	public BasicEnemy(float x, float y, ID id, Handler handler)
	{
		super(x, y, id);
		velX = 5;		// Starts with set velocity
		velY = 5;
		this.handler = handler;
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle((int)x, (int)y, 16, 16);
	}
	
	public void tick()
	{
		x += velX;
		y += velY;
		if (x <= 0 || x >= Game.width - 16)		// Reverses x axis movement when roughly at the edge of the window
			velX *= -1;
		if (y <= 0 || y >= Game.height - 32)	// Reverses y axis movement when roughly at the edge of the window
			velY *= -1;
		handler.addObject(new Trail(x, y, 16, 16, ID.Trail, Color.red, 0.03F, handler));
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.RED);
		g.fillRect((int)x, (int)y, 16, 16);
	}
	
}
