package tutorial.main;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Window extends Canvas
{
	
	private static final long serialVersionUID = 7187823860655836925L;

	//////////////////////////////////////////////////

	
// Window constructor sets up a window for the game, height and width include window borders
	public Window(int width, int height, String title, Game game)
	{
		JFrame frame = new JFrame(title);
		frame.setPreferredSize(new Dimension(width, height));
		frame.setMaximumSize(new Dimension(width, height));
		frame.setMinimumSize(new Dimension(width, height));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null); 					// Sets the window to open in the middle of the screen
		frame.add(game);									// The game has to be added to the window
		frame.setVisible(true);
		game.start();
	}

}
