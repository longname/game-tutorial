package tutorial.main;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter
{
	
	private Handler handler;	// Required to accept key presses for all objects separately
	
	private boolean [] keyDown = new boolean[4];	// 0 = W, 1 = S, 2 = A, 3 = D
	
	////////////////////////////////////////////////////////////////
	
	public KeyInput(Handler handler)
	{
		this.handler = handler;		// Links local handler instance to the handler used in the game class
		for (int i = 0; i < 4; i++)
			keyDown[i] = false;
	}
	
// Key pressed means key held down
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();		// Keyboard keys are represented in numerical values
		for (int i = 0; i < handler.object.size(); i++)		// For every object in the game...
		{
			GameObject tempObject = handler.object.get(i);
			if (tempObject.id == ID.Player)		// Key inputs for the player
			{
				if (key == KeyEvent.VK_W)
				{
					tempObject.setVelY(-5);
					keyDown[0] = true;
				}
				if (key == KeyEvent.VK_S)
				{
					tempObject.setVelY(5);
					keyDown[1] = true;
				}
				if (key == KeyEvent.VK_A)
				{
					tempObject.setVelX(-5);
					keyDown[2] = true;
				}
				if (key == KeyEvent.VK_D)
				{
					tempObject.setVelX(5);
					keyDown[3] = true;
				}
			}
		}
		if (key == KeyEvent.VK_ESCAPE)			// Close on Esc
			System.exit(1);
	}
	
// Self explanatory...
	public void keyReleased(KeyEvent e)
	{
		int key = e.getKeyCode();
		for (int i = 0; i < handler.object.size(); i++)		// See above
		{
			GameObject tempObject = handler.object.get(i);
			if (tempObject.id == ID.Player)
			{
				if (key == KeyEvent.VK_W)
					keyDown[0] = false;
				if (key == KeyEvent.VK_S)
					keyDown[1] = false;
				if (key == KeyEvent.VK_A)
					keyDown[2] = false;
				if (key == KeyEvent.VK_D)
					keyDown[3] = false;
				
				if (!keyDown[0] && !keyDown[1])
					tempObject.setVelY(0);
				if (!keyDown[2] && !keyDown[3])
					tempObject.setVelX(0);
			}
		}
	}
}
