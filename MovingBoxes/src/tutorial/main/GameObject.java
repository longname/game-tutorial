package tutorial.main;

import java.awt.Graphics;
import java.awt.Rectangle;

// Abstract class for most of the interactive in-game objects
public abstract class GameObject 
{
	protected float x, y;
	protected ID id;
	protected float velX, velY;
	
// This constructor is called from within its child(?) class constructors
	public GameObject(float x, float y, ID id)
	{
		this.x = x;
		this.y = y;
		this.id = id;
	}
	
	
	public abstract Rectangle getBounds();
// Handles for tick and render methods in child(?) classes
	public abstract void tick();
	public abstract void render(Graphics g);

	
	// Getters and setters ///////////////////////////////////////////
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getVelX() {
		return velX;
	}

	public void setVelX(float velX) {
		this.velX = velX;
	}

	public float getVelY() {
		return velY;
	}

	public void setVelY(float velY) {
		this.velY = velY;
	}

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}
	//////////////////////////////////////////////////////////////////////
	
	
}
