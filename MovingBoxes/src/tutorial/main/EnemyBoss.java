package tutorial.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

// A class for the first enemy type
public class EnemyBoss extends GameObject
{
	
	private Handler handler;
	private int timer = 70;
	private Random r = new Random();
	
	public EnemyBoss(float x, float y, ID id, Handler handler)
	{
		super(x, y, id);
		velX = 0;		// Starts with set velocity
		velY = 2;
		this.handler = handler;
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle((int)x, (int)y, 128, 128);
	}
	
	public void tick()
	{
		x += velX;
		y += velY;
		
		if (velY != 0)
			if (timer <= 0)
			{
				velY = 0;
				timer = 70;
			}
			else
				timer--;
		else
			if (velX == 0)
				if (timer-- <= 0)
					velX = 3;
				else;
			else
			{
				timer = 300;
				int spawn = r.nextInt(10);
				if (spawn == 0)
					handler.object.add(new EnemyBossBullet(x + 64, y, ID.BasicEnemy, handler));
			}
		if (velX > 0)
			velX += 0.005f;
		if (velX < 0)
			velX -= 0.005f;
		velX = Game.clamp(velX, -20, 20);
		
		if (x <= 0 || x >= Game.width - 134)		// Reverses x axis movement when roughly at the edge of the window
			velX *= -1;
//		if (y <= 0 || y >= Game.height - 158)	// Reverses y axis movement when roughly at the edge of the window
//			velY *= -1;
		handler.addObject(new Trail(x, y, 128, 128, ID.Trail, Color.red, 0.1F, handler));
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.RED);
		g.fillRect((int)x, (int)y, 128, 128);
	}
	
}
